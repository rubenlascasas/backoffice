import React, { Component } from 'react';
import logo from './img/logo.png';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div class="container">
            <section id="treinoa">
              <a href="#treinoa">Treino A</a>
                <p><h4>Exercício</h4>
                  <div class="input">
                    <input type="text" placeholder="Nome do Exercício"/>
                    <input type="text" placeholder="URL do Vídeo"/>
                  </div>
                <h4>Séries e Repetições</h4>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div></p>
            </section>
            <section id="treinob">
              <a href="#treinob">Treino B</a>
                <p><h4>Exercício</h4>
                  <div class="input">
                    <input type="text" placeholder="Nome do Exercício"/>
                    <input type="text" placeholder="URL do Vídeo"/>
                  </div>
                <h4>Séries e Repetições</h4>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div></p>
            </section>
            <section id="planoc">
              <a href="#planoc">Treino C</a>
                <p><h4>Exercício</h4>
                  <div class="input">
                    <input type="text" placeholder="Nome do Exercício"/>
                    <input type="text" placeholder="URL do Vídeo"/>
                  </div>
                <h4>Séries e Repetições</h4>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div></p>
            </section>
            <section id="treinod">
              <a href="#treinod">Treino D</a>
                <p><h4>Exercício</h4>
                  <div class="input">
                    <input type="text" placeholder="Nome do Exercício"/>
                    <input type="text" placeholder="URL do Vídeo"/>
                  </div>
                <h4>Séries e Repetições</h4>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div>
                  <div class="col-rep">
                    <input type="number" placeholder="Rep."/>
                  </div></p>
            </section>
    </div>
        </header>
      </div>
    );
  }
}

export default App;
